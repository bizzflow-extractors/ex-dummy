# ex-dummy

Dummy extractor creating random data

* files - defines what files should be generated - you need to specify files and columns names and columns type also it is possible to specify so columns limitation (described in schema)
* stress_test - it is also possible to define stress test of server with allow to allocate memory (MB) and run for specified number of seconds with approx CPU load in percent (It's very approx)

First tables are created and then stress test is run. Both table generation a and stress test are optional. 
## Example config

```json
{
  "$schema": "schema.json",
  "files": [
    {
      "file_name": "first.csv",
      "row_count": 1000,
      "fields": [
        {
          "name": "a",
          "type": "int",
          "max": 5000,
          "min": 0
        },
        {
          "name": "b",
          "type": "varchar",
          "max_char": 150
        },
        {
          "name": "c",
          "type": "bool"
        }
      ]
    },
    {
      "file_name": "second.csv",
      "row_count": 18,
      "fields": [
        {
          "name": "a",
          "type": "float"
        },
        {
          "name": "b",
          "type": "varchar",
          "alphabet": "aeiouy",
          "max_char": 80
        }
      ]
    }
  ],
  "stress_test": {
    "memory_usage": 2000,
    "run_time": 20,
    "load": 80
  }
}
```
