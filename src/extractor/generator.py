import random
import string
import uuid
from typing import List


class BaseField:
    def __init__(self, name):
        self.name = name

    def get_value(self):
        raise NotImplemented


class FloatField(BaseField):
    def __init__(self, name, min=None, max=None):
        super(FloatField, self).__init__(name)
        self.min = min if min is not None else -10 ** 15
        self.max = max if max is not None else 10 ** 15

    def get_value(self):
        return random.uniform(self.min, self.max)


class IntegerField(FloatField):

    def get_value(self):
        return int(super(IntegerField, self).get_value())


class SequenceField(BaseField):

    def __init__(self, name, start: int):
        super(SequenceField, self).__init__(name)
        start = start or 0
        self.value = start -1

    def get_value(self):
        self.value += 1
        return self.value


class BoolField(IntegerField):

    def get_value(self):
        return bool(super(BoolField, self).get_value() % 2)


class CharField(BaseField):
    def __init__(self, name, alphabet: str = None):
        super(CharField, self).__init__(name)
        self.alphabet = alphabet or (string.ascii_letters + string.digits)

    def get_value(self):
        return str(random.choice(self.alphabet))


class UUIDField(BaseField):

    def get_value(self):
        return str(uuid.uuid4())


class VarCharField(CharField):
    def __init__(self, name, max_char: int, alphabet: str = None):
        super(VarCharField, self).__init__(name, alphabet)
        self.max_char = max_char

    def get_value(self):
        length = random.randint(0, self.max_char)
        return "".join((super(VarCharField, self).get_value() for i in range(length)))


class FieldFactory:

    @staticmethod
    def get_field(**kwargs):
        name = kwargs["name"]
        field_type = kwargs["type"]
        match field_type:
            case "int":
                return IntegerField(name, min=kwargs.get("min"), max=kwargs.get("max"))
            case "bool":
                return BoolField(name)
            case "float":
                return FloatField(name, min=kwargs.get("min"), max=kwargs.get("max"))
            case "uuid":
                return UUIDField(name)
            case "sequence":
                return SequenceField(name, start=kwargs.get("start"))
            case "varchar":
                return VarCharField(name, max_char=kwargs["max_char"], alphabet=kwargs.get("alphabet"))
            case _:
                raise NotImplementedError


class DataGenerator:

    def __init__(self, fields: List[BaseField]):
        self.fields = fields

    def get_header(self):
        return [field.name for field in self.fields]

    def get_row(self):
        return [field.get_value() for field in self.fields]

    def get_rows(self, row_count):
        for i in range(row_count):
            yield self.get_row()
