import csv
from typing import Iterable, List


class Writer:

    @staticmethod
    def save_csv(path: str, header: List, data: Iterable):
        with open(path, "w", encoding="utf-8") as fid:
            writer = csv.writer(fid, dialect=csv.unix_dialect)
            writer.writerow(header)
            writer.writerows(data)
