from extractor.generator import DataGenerator, FieldFactory
from extractor.writer import Writer


class SingleFileExtractor:
    def __init__(self, file_path, fields_definition, row_count):
        self.file_path = file_path
        self.fields_definition = fields_definition
        self.row_count = row_count
        self.generator = self._prepare_generator()

    def _prepare_generator(self):
        fields = [FieldFactory.get_field(**field_def) for field_def in self.fields_definition]
        return DataGenerator(fields=fields)

    def extract_data(self):
        Writer.save_csv(self.file_path, self.generator.get_header(), self.generator.get_rows(self.row_count))