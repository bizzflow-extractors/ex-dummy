import logging
import multiprocessing
import os
import argparse
import json
from math import sqrt
from random import randint
from time import sleep, time
import psutil

from extractor.extractor import SingleFileExtractor

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


def worker(run_time, max_load):
    start_time = time()
    i = 0
    while time() - start_time < run_time:
        i += 1
        if i % (max_load * 1000) == 0 and psutil.cpu_percent() > max_load:
            sleep(0.1)
        sqrt(randint(0, 100000))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", action="store", default="/config/config.json")
    parser.add_argument("--output", action="store", default="/data/out/tables")
    args = parser.parse_args()

    if not os.path.exists(args.output):
        os.makedirs(args.output)

    if not os.path.exists(args.config):
        raise FileNotFoundError(f"Config file {args.config} does not exist")

    with open(args.config, "r", encoding="utf-8") as fid:
        config = json.load(fid)

    for file in config.get("files", []):
        file_path = os.path.join(args.output, file["file_name"])
        fields = file["fields"]
        row_count = file["row_count"]

        logger.info(f"Generating file {file_path}")
        SingleFileExtractor(file_path, fields, row_count).extract_data()
        logger.info(f"File generated {file_path}")

    stress_test = config.get("stress_test")
    if stress_test:
        run_time = stress_test["run_time"]
        load = stress_test.get("load", 100)
        logger.info(f"Running stress test for {run_time}s with approx load {load}%")

        memory_usage = stress_test.get("memory_usage")
        if memory_usage:
            logger.info(f"Allocating {memory_usage}MB of memory")
            allocated_memory = bytearray(memory_usage * 1024 * 1024)

        jobs = []
        for i in range(psutil.cpu_count()):
            p = multiprocessing.Process(target=worker, args=(run_time, load))
            jobs.append(p)
            p.start()


if __name__ == '__main__':
    main()
